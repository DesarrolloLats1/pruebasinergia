import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {
  ProductService
} from "./service.index";


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    ProductService
  ]
})
export class ServicesModule { }
