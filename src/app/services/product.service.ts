import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  BASE_URL: string = environment.apiUrl;
  URI: string = this.BASE_URL + '/Productos';
  constructor(private http: HttpClient, private router: Router) { }

  public getList() {
    return this.http.get(this.URI);
  }

  public getProductById = (id) => {
    return this.http.get(this.URI + "/" + id);
  }

  public create = (body) => {
    return this.http.post(this.URI, body);
  }

  public update = (body) => {
    return this.http.put(this.URI, body);
  }

  public delete = (id) => {
    return this.http.delete(this.URI + "/" + id);
  }

  public getCategorias = () => {
    return this.http.get(this.BASE_URL + '/Categorias');
  }

  public getMarca = () => {
    return this.http.get(this.BASE_URL + '/Marcas');
  }

  public getProveedor = () => {
    return this.http.get(this.BASE_URL + '/Proveedores');
  }

}
