import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Product } from 'src/app/models/product.model';
import { ProductService } from 'src/app/services/product.service';

import Swal from 'sweetalert2';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styles: [
  ]
})
export class ProductComponent implements OnInit {

  public productData: any
  public productForm: FormGroup;
  public product: Product;
  public formSubmitted = false;
  public ProductId: string = '';


  constructor(private fb: FormBuilder, private _productService: ProductService, private router: Router, private activateRoute: ActivatedRoute) {
    this.product = new Product();
  }

  categorias: any;
  marcas: any;
  proveedores: any;
  ProductData: any;

  ngOnInit(): void {

    this.activateRoute.params.subscribe(({ id }) => {
      if (id != 'new') {
        this.getProductById(id);
      }
    });

    this.getCategoria();
    this.getMarca();
    this.getProveedor();

    this.productForm = this.fb.group({
      descripcion: ['', Validators.required],
      categoria: ['', Validators.required],
      proveedor: ['', Validators.required],
      marca: ['', Validators.required],
      medidas: ['', Validators.required],
      cantidad: ['', Validators.required],
      precio_unitario: ['', Validators.required]
    });
  }


  save() {

    this.formSubmitted = true;

    if (this.productForm.valid) {
      console.log("entro");
      let body = {
        descripcion: this.product.descripcion,
        categoria: this.product.categoria,
        proveedor: this.product.proveedor,
        marca: this.product.marca,
        medidas: this.product.medidas,
        cantidad: this.product.cantidad,
        precioUnitario: this.product.precioUnitario
      }

      if (this.ProductId !== "") {

        body['Id'] = this.product.Id;

        console.log("json", body)

        this._productService.update(body).subscribe((data: any) => {

          Swal.fire('Actualizado', `${body.descripcion} Actualizacion exitosa`, 'success');
          this.ProductId = null;
          this.router.navigateByUrl('/productos');
        });

      } else {
        this._productService.create(body)
          .subscribe((data: any) => {
            Swal.fire('Creado', `${body.descripcion} creado correctamente`, 'success');
            this.router.navigateByUrl('/productos');
          })
      }

    }


  }

  InputValidate(input: string): boolean {
    if (this.productForm.get(input).invalid && this.formSubmitted) {
      return true;
    } else {
      return false;
    }
  }

  getCategoria() {
    this._productService.getCategorias().subscribe(data => {
      console.log(data);
      this.categorias = data;
    });
  }

  getProveedor() {
    this._productService.getProveedor().subscribe(data => {
      this.proveedores = data;
    });
  }

  getMarca() {
    this._productService.getMarca().subscribe(data => {
      this.marcas = data;
    });
  }

  getProductById(id: string) {
    this._productService.getProductById(id).subscribe((data:any) => {
      console.log(data[0]);
      this.product = data[0];
      this.product['precioUnitario'] = this.product['precio']
      this.ProductId = this.product.Id;
      this.ProductData = this.product
    })
  }

}
