import { Component, OnInit } from '@angular/core';
import { ProductService } from 'src/app/services/product.service';
import Swal from 'sweetalert2';
import { Product } from './../../models/product.model'
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {

  data: any;

  constructor(private _productService: ProductService,  private router: Router) { }

  ngOnInit(): void {
    this.getProduct();
  }

  getProduct() {
    this._productService.getList().subscribe(data => {
      console.log(data);
      this.data = data;
    }, error => {
      console.log(error);
    })
  }

  DeleteProducto(product: Product) {
    Swal.fire({
      title: '¿Borrar producto?',
      text: `Esta a punto de eliminar a ${product.descripcion}`,
      icon: 'question',
      showCancelButton: true,
      /* confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33', */
      confirmButtonText: 'Si, Eliminarlo'
    }).then((result) => {
      if (result.value) {
        console.log("prod", product);
        this._productService.delete(product.Id).subscribe((resp: any) => {

          Swal.fire(
            'Producto eliminado',
            `${product.descripcion} fue elimando correctamente`,
            'success'            
          )

          this.router.navigate(['/productos']);  

        });
      }
    })    
  }
}
