import { Routes, RouterModule } from '@angular/router';

import { NgModule } from '@angular/core';
import { PagesComponent } from './pages.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product/product.component';

const routes: Routes = [
  {
      path: 'productos',
      component: PagesComponent,
      children: [
          { path: '', component: ProductsComponent, data: { titulo: 'productos' } },
          { path: 'product/:id', component: ProductComponent, data: { titulo: 'Producto' } },
      ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule { }
