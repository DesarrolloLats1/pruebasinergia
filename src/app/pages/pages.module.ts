import { NgModule } from '@angular/core';

import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ProductsComponent } from './products/products.component';
import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NopagefoundComponent } from './nopagefound/nopagefound.component';
import { PagesRoutingModule } from './pages-routing.module';
import { ProductComponent } from './products/product/product.component';


@NgModule({
  declarations: [
    ProductsComponent,
    PagesComponent,
    DashboardComponent,
    NopagefoundComponent,
    ProductComponent
  ],
  exports: [
    ProductsComponent,
    PagesComponent,
    DashboardComponent
  ],
  imports: [
    CommonModule,
    PagesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class PagesModule { }
